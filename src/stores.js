import { writable } from 'svelte/store';

function createGameStore() {
    const { subscribe, set, update } = writable({
        players: [
            { name: '', score: 501, legsWon: 0, currentThrows: [], throwHistory: [], totalThrows: 0, seriesStartScore: 501 },
            { name: '', score: 501, legsWon: 0, currentThrows: [], throwHistory: [], totalThrows: 0, seriesStartScore: 501 }
        ],
        currentPlayerIndex: 0,
        winner: null,
        gameStarted: false
    });

    return {
        subscribe,
        throwDart: (score) => update(state => {
            let currentPlayer = state.players[state.currentPlayerIndex];

            // Score zu Beginn der Serie speichern
            if (currentPlayer.currentThrows.length === 0) {
                currentPlayer.seriesStartScore = currentPlayer.score;
            }

            currentPlayer.currentThrows.push(score);
            let newScore = currentPlayer.score - score;

            if (newScore === 0 && score % 2 === 0) { // Gewinnbedingung
                currentPlayer.legsWon++;
                currentPlayer.score = 0;
                state.winner = state.currentPlayerIndex;
                resetForNewLeg(state);
            } else if (newScore < 0 || newScore === 1 || (newScore === 0 && score % 2 !== 0)) {
                // Score auf den Wert zu Beginn der Serie zurücksetzen
                currentPlayer.throwHistory.push({
                    oldScore: currentPlayer.seriesStartScore,
                    throws: [...currentPlayer.currentThrows],
                    newScore: currentPlayer.seriesStartScore
                });
                currentPlayer.currentThrows = [];
                currentPlayer.score = currentPlayer.seriesStartScore; // Score zurücksetzen
                currentPlayer.totalThrows += 3;
                switchToNextPlayer(state, currentPlayer);
            } else {
                currentPlayer.score = newScore;
                if (currentPlayer.currentThrows.length === 3) {
                    currentPlayer.throwHistory.push({
                        oldScore: currentPlayer.seriesStartScore,
                        throws: [...currentPlayer.currentThrows],
                        newScore: currentPlayer.score
                    });
                    currentPlayer.currentThrows = [];
                    currentPlayer.totalThrows += 3;
                    switchToNextPlayer(state, currentPlayer);
                }
            }

            return state;
        }),
        resetGame: () => set({
            players: [
                { name: '', score: 501, legsWon: 0, currentThrows: [], throwHistory: [], totalThrows: 0, seriesStartScore: 501 },
                { name: '', score: 501, legsWon: 0, currentThrows: [], throwHistory: [], totalThrows: 0, seriesStartScore: 501 }
            ],
            currentPlayerIndex: 0,
            winner: null,
            gameStarted: false
        }),
        setName: (index, name) => update(state => {
            state.players[index].name = name;
            return state;
        }),
        startGame: () => update(state => {
            state.gameStarted = true;
            return state;
        })
    };
}

function resetForNewLeg(state) {
    state.players.forEach(player => {
        player.score = 501;
        player.currentThrows = [];
        player.throwHistory = [];
        player.totalThrows = 0;
        player.seriesStartScore = 501;
    });
}

function switchToNextPlayer(state, currentPlayer) {
    currentPlayer.currentThrows = [];
    currentPlayer.seriesStartScore = currentPlayer.score; // Update series start score for the next turn
    state.currentPlayerIndex = (state.currentPlayerIndex + 1) % state.players.length;
}

export const gameStore = createGameStore();
